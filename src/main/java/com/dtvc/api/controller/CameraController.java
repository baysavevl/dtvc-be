package com.dtvc.api.controller;

import com.dtvc.api.service.CameraService;
import com.dtvc.api.service.GroupCameraService;
import com.dtvc.api.service.LineService;
import com.dtvc.api.service.MatService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import core.constants.AppConstants;
import core.domain.Camera;
import core.domain.GroupCamera;
import core.domain.Line;
import core.dto.CameraDTO;
import core.dto.LineDTO;
import core.dto.LocationDTO;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/camera")
public class CameraController {

    @Autowired
    private CameraService cameraService;

    @Autowired
    private GroupCameraService groupCameraService;

    @Autowired
    private LineService lineService;

    @Qualifier("objectMapper")
    @Autowired
    private com.dtvc.api.mapper.ObjectMapper mapper;

    @GetMapping(value = "/searchLocation")
    public List<LocationDTO> searchLocation(@RequestParam(name = "value", defaultValue = "") String value) {
        Optional<List<String>> list = cameraService.searchLocation(value);
        List<LocationDTO> cameras = cameraService.convertToDTO(list);
        return cameras;
    }

    @GetMapping(value = "/filterByLocationAndStatus")
    public Optional<List<Camera>> filterByLocationAndStatus(@RequestParam(name = "location", defaultValue = "") String location,
                                                            @RequestParam(name = "status", defaultValue = AppConstants.DEFAULT_STATUS) String status,
                                                            @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE + "") int page,
                                                            @RequestParam(name = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE + "") int pageSize) {
        Pageable pageable = PageRequest.of(page - 1, pageSize, Sort.by("group_id"));
        Optional<List<Camera>> list = cameraService.filterByLocationAndStatus(location, status, pageable);
        return list;
    }

    @GetMapping(value = "/getByGroup")
    public List<Camera> getByGroup(@RequestParam(name = "groupId") int groupId) {
        List<Camera> list = cameraService.getByGroup(groupId);
        return list;
    }

    @GetMapping(value = "/searchGroupByLocation")
    public List<GroupCamera> searchGroupByLocation(@RequestParam(name = "location") String location) {
        List<GroupCamera> list = groupCameraService.searchByLocation(location);
        return list;
    }

    @GetMapping(value = "/loadAvailableGroup")
    public Optional<List<GroupCamera>> loadAvailableGroup() {
        Optional<List<GroupCamera>> list = groupCameraService.loadAvailable(AppConstants.MAX_GROUP_SIZE);
        return list;
    }

    /**
     * @param list Example like create(add id of camera and line)
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseEntity update(@RequestBody String list) {
//        int groupId = camera.getGroupCamera().getGroupId();
//        if (groupId == 0) {
//            GroupCamera groupCamera = groupCameraService.create(camera.getGroupCamera());
//            camera.setGroupCamera(groupCamera);
//        }
        try {
            Camera camera = null;
            List<Line> lines = new ArrayList<>();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            Map<String, Object> map = objectMapper.readValue(list,
                    new TypeReference<Map<String, Object>>() {
                    });
            for (Map.Entry entry : map.entrySet()) {
                if (entry.getKey().equals("camera")) {
                    camera = objectMapper.convertValue(entry.getValue(), Camera.class);
                } else {
                    lines.add(objectMapper.convertValue(entry.getValue(), Line.class));
                }
            }
            int groupId = camera.getGroupCamera().getGroupId();
            if (groupId == 0) {
                GroupCamera groupCamera = groupCameraService.create(camera.getGroupCamera());
                camera.setGroupCamera(groupCamera);
            }
            cameraService.update(camera);
            lineService.update(lines);
        } catch (Exception ex) {
        }
        return new ResponseEntity("200", HttpStatus.OK);
    }

    /**
     * param camera
     * param line
     * if group camera id(groupId) == 0 -> create new group camera
     * example json list:
     * {
     * "camera": {
     * "location": "asd",
     * "connectionUrl": "asfsdfd",
     * "position": "left",
     * "groupCamera": {
     * "groupId": 0,
     * "groupName": "sdf"
     * },
     * "status": "active"
     * },
     * "line 1": {
     * "lineType": "a",
     * "top": 10,
     * "left": 101,
     * "right": 12,
     * "bottom": 103
     * },
     * "line 2": {
     * "lineType": "ab",
     * "top": 11,
     * "left": 102,
     * "right": 13,
     * "bottom": 104
     * },
     * "line 3": {
     * "lineType": "abc",
     * "top": 14,
     * "left": 104,
     * "right": 14,
     * "bottom": 123
     * }
     * }
     *
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseEntity create(@RequestBody String list) {
        try {
            Camera camera = null;
            List<Line> lines = new ArrayList<>();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            Map<String, Object> map = objectMapper.readValue(list,
                    new TypeReference<Map<String, Object>>() {
                    });
            for (Map.Entry entry : map.entrySet()) {
                if (entry.getKey().equals("camera")) {
                    camera = objectMapper.convertValue(entry.getValue(), Camera.class);
                } else {
                    lines.add(objectMapper.convertValue(entry.getValue(), Line.class));
                }
            }
            int groupId = camera.getGroupCamera().getGroupId();
            if (groupId == 0) {
                GroupCamera groupCamera = groupCameraService.create(camera.getGroupCamera());
                camera.setGroupCamera(groupCamera);
            }
            Camera newCamera = cameraService.save(camera);
            lines = lineService.setCamera(lines, newCamera);
            lineService.create(lines);
        } catch (Exception ex) {
        }
        return new ResponseEntity("200", HttpStatus.OK);
    }

    @GetMapping(value = "/searchGroupByName")
    public Optional<List<GroupCamera>> searchGroupByName(@RequestParam(name = "value", defaultValue = "") String value) {
        Optional<List<GroupCamera>> list = groupCameraService.searchByName(AppConstants.MAX_GROUP_SIZE, value);
        return list;
    }

    @GetMapping(value = "/getAllByStatus")
    public Optional<List<Camera>> getAllByStatus(@RequestParam(name = "status", defaultValue = AppConstants.DEFAULT_STATUS) String status,
                                                 @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE + "") int page,
                                                 @RequestParam(name = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE + "") int pageSize) {
        Pageable pageable = PageRequest.of(page - 1, pageSize, Sort.by("group_id"));
        Optional<List<Camera>> list = cameraService.getAllByStatus(pageable, status);
        return list;
    }

    @GetMapping(value = "/getAll")
    public Optional<List<Camera>> getAll(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE + "") int page,
                                         @RequestParam(name = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE + "") int pageSize) {
        Pageable pageable = PageRequest.of(page - 1, pageSize, Sort.by("group_id"));
        Optional<List<Camera>> list = cameraService.getAll(pageable);
        return list;
    }

    @GetMapping(value = "/getById")
    public CameraDTO getById(@RequestParam(name = "cameraId") int cameraId) {
        Camera camera = cameraService.getById(cameraId);
        List<Line> lines = lineService.getListByCameraId(cameraId);
        CameraDTO cameraDTO = (CameraDTO) mapper.convertToDTO(camera, CameraDTO.class);
        List<LineDTO> dtoList = mapper.convertToListDTO(lines, LineDTO.class);
        cameraDTO.setLines(dtoList);
        return cameraDTO;
    }


    @PostMapping(value = "/updateStatus")
    public ResponseEntity updateStatus(@RequestParam(name = "cameraId") int cameraId,
                                       @RequestParam(name = "status") String status) {
        int row = cameraService.updateStatus(cameraId, status);
        if (row < 1) {
            return new ResponseEntity("400", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity("200", HttpStatus.OK);
    }

    static {
        String path = null;
        try {
            //I have copied dlls from opencv folder to my project folder
            path = "D:\\FPT_Semester\\Capstone_Project\\new_backend\\api";
            System.load(path + "\\opencv_java430.dll");
            System.load(path + "\\opencv_videoio_ffmpeg430_64.dll");
        } catch (UnsatisfiedLinkError e) {
            System.out.println("Error loading libs");
        }
    }

    @PostMapping("/getImageFromCamera")
    public Map<String, String> getFrameOfCamera(@RequestBody String cameraUrl) {
        JsonObject data = new Gson().fromJson(cameraUrl, JsonObject.class);
        String cameraUrlValue = data.get("cameraUrl").getAsString();

        Map<String, String> imageUrl = new HashMap<>();
        VideoCapture camera = new VideoCapture();
        camera.set(Videoio.CAP_PROP_FRAME_WIDTH, 640);
        camera.set(Videoio.CAP_PROP_FRAME_HEIGHT, 480);
        camera.open(cameraUrlValue);

        if (camera.isOpened()) {
            Mat frame = new Mat();
            if (camera.read(frame)) {
                byte[] imgBytes = MatService.matToStream(frame);
                String imgBase64 = Base64.getEncoder().encodeToString(imgBytes);
                imageUrl.put("frame", imgBase64);
            }
        } else {
            imageUrl.put("frame", "error");
        }
        return imageUrl;
    }

    @GetMapping(value = "/checkGroup")
    public boolean checkGroup(@RequestParam(value = "name", defaultValue = "") String name) {
        boolean isExisted = groupCameraService.checkGroup(name);
        return isExisted;
    }
}
