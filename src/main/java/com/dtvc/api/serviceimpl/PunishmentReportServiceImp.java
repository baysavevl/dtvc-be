package com.dtvc.api.serviceimpl;

import com.dtvc.api.repository.PunishmentReportRepository;
import com.dtvc.api.service.PunishmentReportService;
import core.domain.PunishmentReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PunishmentReportServiceImp implements PunishmentReportService {

    @Autowired
    private PunishmentReportRepository punishmentReportRepository;

    @Override
    public Optional<List<PunishmentReport>> filter(Date fromDate, Date toDate, int violationId, Pageable pageable) {
        Optional<List<PunishmentReport>> list = null;
        if (violationId == -1) {
            list = punishmentReportRepository.filterByDate(fromDate, toDate, pageable);
        } else {
            list = punishmentReportRepository.filter(fromDate, toDate, violationId, pageable);
        }
        return list;
    }

    @Override
    public Optional<List<PunishmentReport>> getAll(Pageable pageable) {
        Optional<List<PunishmentReport>> list = punishmentReportRepository.getAll(pageable);
        return list;
    }

    @Override
    public int create(PunishmentReport punishmentReport) {
        int row = punishmentReportRepository.create(punishmentReport.getCaseId(), punishmentReport.getTrainedStatus(),
                punishmentReport.getCreatedDate(), punishmentReport.getImage().getImageId(), punishmentReport.getLicensePlate(),
                punishmentReport.getLocation(), punishmentReport.getViolationType().getViolationId(), punishmentReport.getReportUrl());
        return row;
    }

    @Override
    public int getCountOfStatus(String status, int violationId) {
        int count = punishmentReportRepository.getCountOfStatus(status, violationId);
        return count;
    }

    @Override
    public Optional<PunishmentReport> getById(int caseId) {
        Optional<PunishmentReport> punishmentReport = punishmentReportRepository.findById(caseId);
        return punishmentReport;
    }

    @Override
    public List<String> getLicense(Date date) {
        List<String> list = punishmentReportRepository.getLicense(date);
        return list;
    }

    @Override
    public Optional<Integer> checkLicense(String license, String location, Date current, int violationId) {
        Optional<Integer> caseId = punishmentReportRepository.checkLicense(license, location, current, violationId);
        return caseId;
    }
}
