package core.constants;

public class AppConstants {

    public static final float LANE_RATE = 20;
    public static final int DEFAULT_PAGE = 1;
    public static final int DEFAULT_VIOLATION_TYPE_ID = 1; // is not wearing helmet
    public static final int DEFAULT_VIOLATION_TYPE = -1; // is all violation types
    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final int MAX_GROUP_SIZE = 2;
    public static final long DEFAULT_SUGGESTION_TIME = 30000; // 30 seconds
    public static final long DEFAULT_APPROVE_TIME = 1800000; // 30 minutes
    public static final String DEFAULT_STATUS = "all";
    public static final String ACTIVE_STATUS = "active";
    public static final String NOT_TRAINED_STATUS = "inactive";
    public static final String DEFAULT_CASE = "unconfirmed";
    public static final String REJECTED_CASE = "rejected";
    public static final String APPROVED_CASE = "punishment";
    public static final String EMAIL = "chienndse63337@gmail.com";
    public static final String PASSWORD = "Chien000";
    public static final String SUBJECT = "Verify account";
    public static final String HOST = "http://localhost:8080/confirm";
    public static final String PROJECT_ID = "capstone-dtv";
    public static final String BUCKET_NAME = "capstone-dtv.appspot.com";
//    public static final String DATABASE_URL = "https://capstone-dtv.firebaseio.com";
//    public static final int DEFAULT_ROLE_ID = 2;//is moderator role
}
